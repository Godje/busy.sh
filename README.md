# busy.sh

Efficiency-tracking script akin to [Log by Josh Avanier](https://avanier.dev/log) (LJA for the rest of the document). It records data to the `$BUSYFILE` in the `csv` format. Which means you can export this piece of data into Excel/Google Sheets to create cool graphs for yourself.

Content:
- [Usage](#usage)
- [Thoughts](#thoughts)
- [LJA-similarity](#lja-similarity)
- [TODO](#todo)
- [Contribution](#contribution)

## Usage

```
Usage: busy [action] [arguments]

Actions:
  start
    Launches an interactive menu for the "busy create" command

  create
    Creates a busy entry in the $BUSYFILE
    Usage: start <activity> <project> <details>

    activity - activity that could be common across many projects (Ex. Research, Debug)
    project - Project name
    details - Details about current session

  end
    Ends an entry

  resume
    Copies the Activity, Project, and Details property of the last entry and
    starts a new busy entry with those arguments

  print
    Prints last <number> (default: 10) entries out

    Usage: print <number>

  help
    Displays this text

```

## Thoughts
This script is **meant to be alias'd** to your workflow, not used raw. No need to I re-invent the wheel with dedicated config files, if it's all possible within bash alias or functions.

```bash
alias bgamestart="busy create Programming GameProject" # followed by ex. "debugging character movement" or something
alias bread="busy create Reading ENG201" # followed by ex. "chapter 23: commas" or something

# function example, for the epic nerds
b(){
	case "$1" in
		'math') busy create Math MATH208 "must write 10 proofs or mom dies";;
		'engwrite') busy create Writing ENG201 "research paper evidence collection";;
		'engread') busy create Reading ENG201 "$2";; # followed by ex. "chapter 23: commas" or something
		*) busy print 12;;
	esac

	# in this same function you could also insert your own commands for editing
	# the .csv file in the way that busy doesn't currently let you
	# ...
}
```

I might create some cool stuff in the future to see a cool vizualisation of all the data, when I get way too bored with my life, but Excel should do for now.

### LJA similarity
Currently the software is sticking to the same column-count as LJA, like "Activity", "Project", "Details". However in the future I might re-organize the script to be able to handle as many columns as you want, but to be honest, I have no idea who'd need more metadata than this. Also need to create a better "print" function so it at least slightly resembles LJA's [TUI screenshot](https://avanier.dev/m/log3.png). Also "resume" would be a cool command, I should implement that one next.

## TODO
- better print look
- "resume" function that will just take last entry's arguments and starts it again.
- an "installation" `make` script to add the script to `$PATH`.

## Contribution
- Take anything on the TODO list.
- Or add any other cool feature you think would be cool.
- Some sort of stylistic changes to the shell script are also welcome, cause I'm not an expert in this and would appreciate any sort of help with getting better at CLI tool stuff
- Conversion to POSIX compliance. This is an insane ask, but if you feel like it, I won't stop you :)
